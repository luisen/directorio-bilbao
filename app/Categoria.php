<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    // Leer las rutas por slug
    public function getRouteKeyName()
    {
        return 'slug';
    }

    // Relación 1:N categoria - establecimientos
    public function establecimientos()
    {
        return $this->hasMany(Establecimiento::class);
    }
}
