<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Establecimiento;
use App\Imagen;

class APIController extends Controller
{
    // Método para obtener todos los establecimientos
    public function index()
    {
        $establecimientos = Establecimiento::with('categoria')->get();
        return response()->json($establecimientos);
    }

    // Método para obtener todos las categorias
    public function categorias()
    {
        $categorias = Categoria::all();

        // Cuando creamos una api, las respuestas tienen que estar en JSON
        return response()->json($categorias);
    }

    // Muestra los establecimientos por la categoría en específico

    public function categoria(Categoria $categoria)
    {
        $establecimnientos = Establecimiento::where('categoria_id', $categoria->id)->with('categoria')->take(3)->get();

        return response()->json($establecimnientos);
    }

    public function establecimientocategoria(Categoria $categoria)
    {
        $establecimnientos = Establecimiento::where('categoria_id', $categoria->id)->with('categoria')->get();

        return response()->json($establecimnientos);

    }

    // Muestra un establecimiento en específico
    public function show(Establecimiento $establecimiento)
    {
        $imagenes = Imagen::where('id_establecimiento', $establecimiento->uuid)->get();

        $establecimiento->imagenes = $imagenes;

        return response()->json($establecimiento);
    }
}
