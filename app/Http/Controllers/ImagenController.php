<?php

namespace App\Http\Controllers;

use App\Establecimiento;
use App\Imagen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ImagenController extends Controller
{
    //
    public function store(Request $request)
    {
        // leer la imagen
        // Cuanto utilizas store, hay que instalar storage:link en Laravel
        $ruta_imagen = $request->file('file')->store('establecimientos', 'public');

        // Resize a la imagen
        $imagen = Image::make(public_path("storage/{$ruta_imagen}"))->fit(800, 450);

        $imagen->save(); // guardar imagen en disco duro servidor

        // Almacenar con modelo
        $imagenDB = new Imagen(); //añadir imagen con modelo Imagen
        // Referencia imagenes a que establecimiento perteneces con uuid
        $imagenDB->id_establecimiento = $request['uuid'];
        $imagenDB->ruta_imagen = $ruta_imagen;
        $imagenDB->save(); //lo guardamos en la BD

        // Retornar respuesta
        $respuesta = [
            'archivo' => $ruta_imagen,
        ];

        // return $request->all(); // all(), para acceder la información de texto plano(no archivo)
        // return $request->file('file'); // file(), para acceder la información archivo

        return response()->json($respuesta);
    }

    // Eliminar imagen de la base de datos
    public function destroy(Request $request)
    {
        // Validación
        $uuid = $request->get('uuid'); // lo coge de /data/imagen
        $establecimiento = Establecimiento::where('uuid', $uuid)->first();
        $this->authorize('delete', $establecimiento);

        // Imagen a eliminar
        $imagen = $request->get('imagen'); // lo coge de /data/imagen

        if (File::exists('storage' . $imagen)) {

            // Elimina imagen del servidor
            File::delete('storage' . $imagen);

            // Elimina imagen de la db
            Imagen::where('ruta_imagen', $imagen)->delete();
        }

        // Retornar respuesta
        $respuesta = [
            'mensaje' => 'imagen Eliminada',
            'imagen' => $imagen,
        ];

        // Eliminación de la BD
        Imagen::where('ruta_imagen', '=', $imagen)->delete();

        return response()->json($respuesta);

    }
}
