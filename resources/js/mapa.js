import { GeoSearchControl, OpenStreetMapProvider } from "leaflet-geosearch";

const provider = new OpenStreetMapProvider();

document.addEventListener("DOMContentLoaded", () => {
    if (document.querySelector("#mapa")) {
        const lat =
            document.querySelector("#lat").value === ""
                ? 43.2633534
                : document.querySelector("#lat").value;
        const lng =
            document.querySelector("#lng").value === ""
                ? -2.951074
                : document.querySelector("#lng").value;

        const mapa = L.map("mapa").setView([lat, lng], 16);

        // Eliminar pines previos
        let markers = new L.FeatureGroup().addTo(mapa);

        L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
            attribution:
                '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(mapa);

        let marker;

        // agregar el pin
        marker = new L.marker([lat, lng], {
            draggable: true,
            autoPan: true //permite que se vaya moviendo el mapa
        }).addTo(mapa);

        // Agregar el pin a las capas
        markers.addLayer(marker);

        // Geocode Service
        const geocodeService = L.esri.Geocoding.geocodeService();

        const buscador = document.querySelector("#formbuscador");

        buscador.addEventListener("blur", buscarDireccion);

        reubicarPin(marker);

        function reubicarPin(marker) {
            // Detectar movimiento dle marker
            marker.on("moveend", function(e) {
                marker = e.target;

                const posicion = marker.getLatLng();

                // Centrar automáticamente
                mapa.panTo(new L.LatLng(posicion.lat, posicion.lng));

                // Reverse Geocoding, cuando el usuario reubica el pin
                geocodeService
                    .reverse()
                    .latlng(posicion, 16)
                    .run(function(error, resultado) {
                        // console.log(error);

                        // console.log(resultado.address);

                        marker.bindPopup(resultado.address.LongLabel);
                        marker.openPopup();

                        // Llenar los campos
                        llenarInput(resultado);
                    });
            });
        }

        function llenarInput(resultado) {
            console.log(resultado);
            document.querySelector("#direccion").value =
                resultado.address.Address || "";
            document.querySelector("#zona").value =
                resultado.address.Neighborhood || "";
            document.querySelector("#lat").value = resultado.latlng.lat || "";
            document.querySelector("#lng").value = resultado.latlng.lng || "";
        }

        function buscarDireccion(e) {
            if (e.target.value.length > 1) {
                provider
                    .search({ query: e.target.value + " Bilbao ES " })
                    .then(resultado => {
                        if (resultado) {
                            // Limpiar los pines
                            markers.clearLayers();

                            // Reverse Geocoding, cuando el usuario reubica el pin
                            geocodeService
                                .reverse()
                                .latlng(resultado[0].bounds[0], 16)
                                .run(function(error, resultado) {
                                    // Llenar los inputs
                                    llenarInput(resultado);

                                    // Centrar el mapa
                                    mapa.setView(resultado.latlng);

                                    // Agregar el pin
                                    marker = new L.marker(resultado.latlng, {
                                        draggable: true,
                                        autoPan: true //permite que se vaya moviendo el mapa
                                    }).addTo(mapa);

                                    // asignar el contenedor de markers el nuevo pin
                                    markers.addLayer(marker);

                                    // Movel el pin
                                    reubicarPin(marker);
                                });
                        }
                    })
                    .catch(error => {
                        console.log(error);
                    });
            }
        }
    }
});
